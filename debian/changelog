wmcliphist (2.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/control
    - Bump debhelper from old 12 to 13.
  * debian/copyright
    - Use spaces rather than tabs to start continuation lines.
  * debian/rules
    - Avoid explicitly specifying -Wl,--as-needed linker flag.

  [ Doug Torrance ]
  * debian/control
    - Update Maintainer email address to use tracker.d.o.
    - Bump Standards-Version to 4.6.0.
    - Reformat using wrap-and-sort.
  * debian/copyright
    - Update upstream mailing list address.
    - Use https for Source URL.
  * debian/menu
    - Remove deprecated Debian menu file.
  * debian/patches/*.patch
    - Add Forwarded fields.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 10 Oct 2021 06:52:40 -0400

wmcliphist (2.1-3) unstable; urgency=medium

  [ Doug Torrance ]
  * Update Vcs-* after migration to Salsa.
  * Update Format in d/copyright to https.
  * Switch Priority from extra to optional.
  * Update homepage to dockapps.net.

  [ Jeremy Sowden ]
  * Bump standards-version to 4.4.0.
  * Bump compat to 12 and replace build-dep on debhelper with debhelper-compat.
  * Add myself to uploaders.
  * Remove get-orig-source from d/rules.
  * Add hardening to d/rules.
  * Update dockapps URL's to https.
  * Fix FTCBFS by adding patch to make pkg-config configurable.
    Closes: #941312

  [ Andreas Metzler ]
  * Set Rules-Requires-Root: no
  * Run "wrap-and-sort --max-line-length=72 --short-indent".
  * Add myself to uploaders.

 -- Andreas Metzler <ametzler@debian.org>  Sun, 29 Sep 2019 06:54:06 +0200

wmcliphist (2.1-2) unstable; urgency=medium

  * debian/control
    - Update Vcs-* fields.
    - Set Debian Window Maker Team as Maintainer; move myself to
      Uploaders with updated email address.
  * debian/{copyright,patches}
    - Update email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 17 Aug 2015 22:02:53 -0400

wmcliphist (2.1-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #533373).
  * debian/compat
    - Bump to 9.
  * debian/control
    - Bump versioned dependency on debhelper to >=9.
    - Add Homepage and Vcs-* fields.
    - Remove Build-Depends on dockbook-to-man and dpatch.
    - Replace Build-Depends on libglib1.2-dev and libgtk1.2-dev with
      libgtk-3-dev.
    - Add ${misc:Depends} to Depends.
    - Bump Standards-Version to 3.9.6.
  * debian/copyright
    - Update to DEP 5 (version 1.0) format.
  * debian/dirs
    - Remove unnecessary file.
  * debian/docs
    - Remove debian/wmcliphistrc; nonexistent file.
  * debian/examples
    - New file; add wmcliphistrc.
  * debian/menu
    - Update section.
    - Quote strings.
  * debian/patches
    - (deprecated-declarations.patch, maybe-uninitialized.patch) Fix compiler
      warnings.
  * debian/rules
    - Update to use dh.
    - Add get-orig-source target.
    - Add "-Wl,--as-needed" to LDFLAGS; avoids "useless dependency" warnings
      from dpkg-shlibdeps.
  * debian/source/format
    - New file; use 3.0 (quilt).
  * debian/watch
    - New file.
  * debian/wmcliphist.sgml
    - Remove file; was used to generate manpage, which is now included upstream.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Sun, 23 Nov 2014 12:53:22 -0600

wmcliphist (0.6-4) unstable; urgency=low

  * Orphan the package.

 -- Sebastian Ley <ley@debian.org>  Sat, 08 Sep 2007 16:21:48 +0200

wmcliphist (0.6-3) unstable; urgency=low

  * Drop the build dependency on xlibs-dev (Closes: #347114).

 -- Sebastian Ley <ley@debian.org>  Tue, 10 Jan 2006 13:01:13 +0100

wmcliphist (0.6-2) unstable; urgency=low

  * Add -s option to manpage (Closes: #244082).

 -- Sebastian Ley <ley@debian.org>  Fri, 16 Apr 2004 19:25:27 +0200

wmcliphist (0.6-1) unstable; urgency=low

  * New upstream release.
    - Upstream applied Micheal Beattie's patch for better execution handling
      (Closes: #204151).
  * Change maintainer field to my debian.org address.
  * Update Standards-Version to 3.6.1.0 (no changes).

 -- Sebastian Ley <ley@debian.org>  Tue,  2 Sep 2003 00:46:48 +0200

wmcliphist (0.5-1) unstable; urgency=low

  * New upstream release.
  * Remove patch for the Makefile. Upstream has incorporated this.

 -- Sebastian Ley <sebastian.ley@mmweg.rwth-aachen.de>  Mon, 30 Jun 2003 00:05:10 +0200

wmcliphist (0.4-1) unstable; urgency=low

  * New upstream release.
  * Remove patch to generate an rc file. Upstream has incorporated this.
  * Correct copyright information. wmcliphist is published under the GPL.
  * Update Standards-Version to 3.5.10 (no changes).
  * Install a sample wmcliphistrc file into /usr/share/doc/wmcliphist and post
    a note about this in the manpage (Closes: #198972).

 -- Sebastian Ley <sebastian.ley@mmweg.rwth-aachen.de>  Thu, 12 Jun 2003 15:26:23 +0200

wmcliphist (0.3-1) unstable; urgency=low

  * Initial release (Closes: #187438).

 -- Sebastian Ley <sebastian@coyote.mmweg.rwth-aachen.de>  Tue,  8 Apr 2003 23:45:20 +0200
